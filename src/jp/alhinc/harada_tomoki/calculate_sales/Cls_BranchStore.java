package jp.alhinc.harada_tomoki.calculate_sales;

public class Cls_BranchStore {
	// 固定値
	private final Integer ReadIndex_No = 0;
	private final Integer ReadIndex_Name = 1;

	// フィールド
	public String No;
	public String Name;
	public Integer Sales_Amount;

	// プロパティ
	public Cls_BranchStore(String[] ReadedStoreInfo)
	{
		this.No = ReadedStoreInfo[ReadIndex_No];
		this.Name = ReadedStoreInfo[ReadIndex_Name];
		this.Sales_Amount = 0;
	}

	//売上を集計
	public void Add_Sales_Amount(Integer Amount){
		this.Sales_Amount = this.Sales_Amount + Amount;
	}

	// 支店別集計ファイル出力
	public String Get_StoreSalesInfo(){
		return this.No + "," + this.Name + "," + this.Sales_Amount.toString() + "\r\n";
	}
}
