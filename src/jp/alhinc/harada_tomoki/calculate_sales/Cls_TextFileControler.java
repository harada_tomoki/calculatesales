package jp.alhinc.harada_tomoki.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Cls_TextFileControler {
	// フィールド
	private String FilePath;
	private File FileObject;
	public List<String> FileLines;

	// エラーメッセージ
	public String ErrMsg;

	// コンストラクタ
	public Cls_TextFileControler(String ArgPath){
		this.FilePath = ArgPath;
	}
	public Cls_TextFileControler(File ArgFile){
		this.FileObject = ArgFile;
		this.FileLines = this.Read_FileLines();
	}

	// テキストファイルの読み込み
	private List<String> Read_FileLines(){
		List<String> Result = new ArrayList<String>();
		FileReader TextReader;
		BufferedReader bufReader;
		try{

			// ファイルを読み込む
			TextReader = new FileReader(this.FileObject);
			bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(this.FileObject),"UTF-8"));
			String Line = bufReader.readLine();;
			while ((Line != null)){
				Result.add(Line);
				Line = bufReader.readLine();
			}

			// ファイルを閉じる
			TextReader.close();
			bufReader.close();

			// 結果を返す
			return Result;

		}catch(Exception ex){
			this.ErrMsg = ex.getMessage();
			return new ArrayList<String>();
		}
	}

	public boolean Write_FileLines(List<String> OutputData){
		FileWriter Writer = null;
		BufferedWriter bufWriter;

		// 出力内容を保存
		this.FileLines = OutputData;

		try{
			// 書き込みの準備
			FileObject = new File(this.FilePath);
			Writer = new FileWriter(FileObject);
			bufWriter = new BufferedWriter(Writer);

			// 一行ずつ書き込み
			for(String FileLine : OutputData){
				bufWriter.write(FileLine);
			}

			// ファイルを閉じる
			bufWriter.close();
			Writer.close();

			// 結果を返す
			return true;

		}catch(Exception ex){
//			System.out.println("ファイルの作成に失敗しました");
			return false;
		}
	}
}
