package jp.alhinc.harada_tomoki.calculate_sales;

public class Cls_Main {

	public static void main(String[] args) {
		try{
			// TODO 自動生成されたメソッド・スタブ

			// 集計クラスを生成
			Cls_CalculateSales CalculateSales = new Cls_CalculateSales();

			// 集計処理実行
			String EndMsg = "";
			EndMsg = CalculateSales.Begin_Calculate(args[0]);

			if (EndMsg.length() != 0){
				System.out.println(EndMsg);
			}
		}catch(Exception ex){
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
