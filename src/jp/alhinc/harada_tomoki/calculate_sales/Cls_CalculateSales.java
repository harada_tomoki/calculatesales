package jp.alhinc.harada_tomoki.calculate_sales;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cls_CalculateSales {
	// ファイル拡張子の固定値
	private final String SalesExtension = ".rcd";	// 売上
	private final String StoresExtension = "branch.lst";	// 支店定義ファイル
	private final String Sales_by_Branch_FileName = "branch.out";	// 支店別集計
	private final int SALES_FILENAME_LENGTH = 8;

	private File StoreListFile;
	private List<File> StoreSaleFiles;

	// 店舗情報のディクショナリ
	Map<String,Cls_BranchStore> BranchStoreMap;

	public String ErrMsg;

	// コンストラクタ
	public Cls_CalculateSales(){
		ErrMsg = "";
	}

	// 集計処理メイン
	public String Begin_Calculate(String FolderPath){
		try{
			// 各ファイルを取得
			if (!Get_CalculateFiles(FolderPath)){
				return ErrMsg;
			}

			// 支店ディレクトリの生成
			this.BranchStoreMap = Get_BranchStoreMap(this.StoreListFile);
			if (this.BranchStoreMap == null){
				return ErrMsg;
			}

			// 店舗別に売上を集計
			if (!Execute_Calculate(this.StoreSaleFiles)){
				return ErrMsg;
			}

			// 支店別売上ファイルを生成
			if (!Create_Sales_by_Branch_File(FolderPath)){
				return ErrMsg;
			}

			return ErrMsg;

		}catch(Exception ex){
			return "予期せぬエラーが発生しました";
		}
	}

	// 対象ファイルの取得
	private boolean Get_CalculateFiles(String FolderPath){
		// 指定ディレクトリ配下のファイル一覧を取得
		File DirectoryFile = new File(FolderPath);
		File[] Files = DirectoryFile.listFiles();
		Boolean Result = true;

		// 売上集計に必要なファイルを取得
		List<File> FileList = new ArrayList<File>();
		Arrays.asList(Files).forEach(File ->
		{
			// 支店定義ファイル
			if (File.getName().endsWith(StoresExtension)){
				StoreListFile = File;
			}

			// 売上
			String filename = File.getName();
			if ((filename.endsWith(SalesExtension)) &&
				(filename.replaceAll(SalesExtension, "").length() == SALES_FILENAME_LENGTH)){
				FileList.add(File);
			}
		});

		this.StoreSaleFiles = FileList;
		return Result;
	}

	// 支店マップの生成
	private Map<String,Cls_BranchStore> Get_BranchStoreMap(File ArgFile){
		if (!ArgFile.exists()){
			ErrMsg = "支店定義ファイルが存在しません";
			return null;
		}

		// テキスト操作クラス
		Cls_TextFileControler FileControler = new Cls_TextFileControler(ArgFile);

		// フォーマットチェック
		if (!Check_Branch_File_Format(FileControler.FileLines)){
			ErrMsg = "支店定義ファイルのフォーマットが不正です";
			return null;
		}

		// IDチェックはしていない
		Map<String,Cls_BranchStore> Result = new HashMap<String,Cls_BranchStore>();
		FileControler.FileLines.forEach(StoreInfo -> {
			if (StoreInfo == null) return;
			Cls_BranchStore Store = new Cls_BranchStore(StoreInfo.split(","));
			Result.put(Store.No, Store);
		});

		// 同じコードが入っている場合上書きされてしまうのでエラーとして処理する
		if (FileControler.FileLines.size() != Result.size()){
			ErrMsg = "予期せぬエラーが発生しました";
			return null;
		}

		return Result;
	}

	// 支店別に売上を集計
	private boolean Execute_Calculate(List<File> ArgFileList){
		final Integer Index_No = 0;
		final Integer Index_Amount = 1;

		Integer LastFileNo = 0;
 		Integer AddFileNo = 0;

		// 売上を集計
		for (int index = 0;index < ArgFileList.size();index++){
			File ArgFile = ArgFileList.get(index);

			// 売上ファイルが連番かどうか
			AddFileNo = ToInteger(ArgFile.getName().replaceAll(SalesExtension, ""));
			if ((LastFileNo + 1) != AddFileNo){
 	 			this.ErrMsg = "売上ファイル名が連番になっていません";
 	 			return false;
 	 		}

			Cls_TextFileControler FileControler = new Cls_TextFileControler(ArgFile);
			String StoreNo = FileControler.FileLines.get(Index_No);
			String Amount = FileControler.FileLines.get(Index_Amount);

			// 売上ファイルのフォーマットチェック
			if (FileControler.FileLines.size() != 2){
				ErrMsg = ArgFile.getName() + "のフォーマットが不正です";
				return false;
			}

			// 該当店舗の存在確認
			if (!BranchStoreMap.containsKey(StoreNo)){
				ErrMsg = ArgFile.getName() + "の支店コードが不正です";
				return false;
			}

			// 売上を追加
			BranchStoreMap.get(StoreNo).Add_Sales_Amount(ToInteger(Amount));

			// 10 桁以上でエラー
			if (String.valueOf(BranchStoreMap.get(StoreNo).Sales_Amount).length() > 9){
				ErrMsg = "合計金額が10桁を超えました";
				return false;
			}

			// 最終カウントを更新
			LastFileNo = AddFileNo;
		}

		return true;
	}

    // 支店別売上集計ファイルを作成
	private boolean Create_Sales_by_Branch_File(String ArgFolderPath){

    	// 集計ファイルへの記載内容を生成
    	List<String> OutputData = new ArrayList<String>();
		this.BranchStoreMap.forEach((No,Store) ->
		{
			OutputData.add(Store.Get_StoreSalesInfo());
		});

		// 集計ファイル出力
		String FilePath = new File(ArgFolderPath, this.Sales_by_Branch_FileName).getPath();
		Cls_TextFileControler FileControler = new Cls_TextFileControler(FilePath);
		boolean result = FileControler.Write_FileLines(OutputData);

		// 集計結果を画面に表示
		StringBuilder outValue = new StringBuilder();
		FileControler.FileLines.forEach(line ->
		{
			outValue.append(line);
		});

		System.out.println(outValue.toString());

		return result;

    }

 	// 支店定義ファイルのフォーマットチェック(Validation)
 	private boolean Check_Branch_File_Format(List<String> StoresInfo){
 		int index = 0;
 		String[] StoreInfoLine;
 		for(;index < StoresInfo.size() -1;index++)
 		{
 			StoreInfoLine = StoresInfo.get(index).split(",");

 			// 一行あたりのデータ数
 			if (StoreInfoLine.length != 2){
 				return false;
 			}

 			// 店舗の桁数
 			if (StoreInfoLine[0].length() != 3){
 				return false;
 			}

 			// 数字のみか
 			try{
 				Integer.parseInt(StoreInfoLine[0]);
 			}catch(Exception ex){
 				return false;
 			}
 		}

 		return true;
 	}

 	// 文字 → 数字
 	private Integer ToInteger(String Text){
		try{
			return Integer.parseInt(Text);
		}catch(Exception ex){
			ErrMsg = "売上の集計中に不正なファイルが存在しました";
			return 0;
		}
	}

}
